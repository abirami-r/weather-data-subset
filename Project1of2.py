#!/usr/bin/env python3.8
# coding: utf-8
import pandas as pd
import argparse
import csv


def process(infile, outfile, date_from, date_to):
    df = pd.read_csv(infile, parse_dates=[4])
    from_year, from_month, from_day = date_from[0], date_from[1], date_from[2]
    to_year, to_month, to_day = date_to[0], date_to[1], date_to[2]
    date_range = (f"{from_year}-{from_month}-{from_day}",
                  f"{to_year}-{to_month}-{to_day}")
    date_mask = df.iloc[:, 4].between(date_range[0], date_range[1])
    out = df.loc[date_mask]
    out.to_csv(outfile, index=False, quoting=csv.QUOTE_ALL)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", type=str)
    parser.add_argument("date_from", type=int, nargs=3, help='start date as YYYY MM DD')
    parser.add_argument("date_to", type=int, nargs=3, help='end date as YYYY MM DD') 
    parser.add_argument("--outfile", "-o", default="out.csv")
    args = parser.parse_args()
    process(args.infile, args.outfile, args.date_from, args.date_to)
